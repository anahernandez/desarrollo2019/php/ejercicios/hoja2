<?php
if($_REQUEST){
    $mal=false;
} else {
    $mal=true;
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        if($mal){
        ?>
            <form name="f">
                <label for="ciudad">
                    Selecciona la ciudad
                </label>
                <select id="ciudad" name="ciudad">
                    <optgroup label="Asia">
                        <option value="1">Delhi</option>
                        <option value="2">Hong Kong</option>
                        <option value="3">Mumbai</option>
                        <option value="4">Tokyo</option>
                    </optgroup>
                    
                    <optgroup label="Europe">
                        <option value="5">Amsterdam</option>
                        <option value="6">London</option>
                        <option value="7">Moscow</option>
                    </optgroup>
                    
                    <optgroup label="North America">
                        <option value="8">Los Angeles</option>
                        <option value="9">New York</option>
                    </optgroup>
                    
                     <optgroup label="South America">
                         <option value="10">Buenos Aires</option>
                        <option value="11">Sao Paulo</option>
                    </optgroup>
                    
                </select>
                     
            <input type="submit" value="Enviar" name="boton"/>
        </form>
        <?php
        } else {

        $ciudades=["","Delhi","Hong Kong", "Mumbai", "Tokyo", "Amsterdam", "London", "Moscow", "Los Angeles","New York", "Buenos Aires","Sao Paulo"];
        echo "La ciudad seleccionada es: ";
        echo $ciudades[$_REQUEST['ciudad']];
        
        }
        ?>
    </body>
</html>
